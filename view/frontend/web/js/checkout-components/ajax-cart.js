/**
 * Elogic Wishlist - Ajax wishlist
 * Ajax qty update functionality
 */

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('elogic.ajaxCart', {
        options: {
            selectors: {
                formCart: '.form-cart',
                inputQty: '.form-cart input.qty',
                qtyControl: '.form-cart input.qty'
            }
        },

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            var self = this;

            self._cartQtyUpdate();
        },

        _cartQtyUpdate: function () {
            var self = this;

            $(document).on('change', '.form-cart input.qty', function () {
                self._sendAjax();
            });

            $(document).on('click', '.form-cart .qty-control', function () {
                self._sendAjax();
            });
        },


        /**
         * Update QTY
         *
         * @private
         */
        _sendAjax: function() {
            var selectors = this.options.selectors,
                form = $(selectors.formCart);

            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                showLoader: true,

                success: function (res) {
                    var parsedResponse = $.parseHTML(res);
                    var result = $(parsedResponse).find('.form-cart');
                    var sections = ['cart'];

                    form.replaceWith(result);

                    require([
                        'Magento_Checkout/js/action/get-totals',
                        'Magento_Customer/js/customer-data'
                    ], function (getTotalsAction, customerData) {
                        // The mini cart reloading
                        customerData.reload(sections, true);

                        // The totals summary block reloading
                        var deferred = $.Deferred();
                        getTotalsAction([], deferred);

                        // Display error if found after jquery
                        var messages = $.cookieStorage.get('mage-messages');
                        if (!_.isEmpty(messages)) {
                            customerData.set('messages', {messages: messages});
                            $.cookieStorage.set('mage-messages', '');
                        }
                    });
                },

                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        }
    });

    return $.elogic.ajaxCart;
});
