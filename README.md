# Elogic Ajax Shopping Cart module

> Updating product quantity in shopping cart via AJAX

## Description

AJAX Shopping Cart Update is built to update the quantity of the customer’s order seamlessly. Total price adjustments are visible on the shopping cart page instantly. There are no needless page reloads. 
There are buttons to increase or decrease the quantity of items on your cart. It is a much simpler approach for updating the products on cart. Customers don’t have to manually enter the quantity in the cart, if they don’t want to. They don’t have to click ‘Update to Cart’ button every time they update item's quantity. This makes the whole process intuitive and flawless.  

Here are some of the salient features for the extension:
1. AJAX-based technology to let customers update quantity of the products to the cart\
2. Buttons to increase and decrease the quantity of items
3. AJAX call to cart, summary and mini-cart to automatically update the price without page reload

## Installation

1. Go to Magento2 root folder 
2. Require this extension, enter following command to install extension: ```composer require elogic/module-ajax-shopping-cart```
3. Wait while composer is updated.

### OR

You can also download code from our repo:
https://gitlab.com/elogic-group/elogic-ajax-shopping-cart

Enter following commands to enable the module:

```
php bin/magento module:enable Elogic_AjaxShoppingCart
php bin/magento setup:upgrade
```

## Requirements

Magento installation

## Compatibility

Extension works for both magento- and breeze-based themes

## TODO list:

1. ~~Move template from theme to module~~
2. Add AJAX update for mini cart qty controls
3. Add AJAX for item removing
