<?php

declare(strict_types=1);

namespace Elogic\AjaxShoppingCart\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Helper extends AbstractHelper
{
    const SHOPPING_CART_CONFIG_PATH = 'ajax_shopping_cart/general/enable';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor
     *
     * @param Context $context
     * @param \Magento\Wishlist\Model\Wishlist $wishlist
     * @param \Magento\Customer\Model\Session $customer
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Get is AJAX Shopping Cart enabled
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isShoppingCartEnabled() : bool
    {
        return $this->scopeConfig->isSetFlag(
            self::SHOPPING_CART_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }
}
